'use strict';

const path = require('path'),
    gulp = require('gulp'),
    webpackStream = require('webpack-stream'),
    webpack = webpackStream.webpack,
    gulpIf = require('gulp-if'),
    del = require('del'),
    gulpSequence = require('gulp-sequence'),
    browserSync = require('browser-sync').create(),
    revReplace = require('gulp-rev-replace'),
    AssetsPlugin = require('assets-webpack-plugin'),
    isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('assets', () => {
    return gulp.src('app/templates/**/*.*')
        .pipe(gulpIf(!isDevelopment, revReplace({
            manifest: gulp.src('manifest/css.json', {allowEmpty: true})
        })))
        .pipe(gulp.dest('build'));
});

gulp.task('webpack', (callback) => {
    let isReadyFirstBuild = false;

    function done(error, stats) {
        isReadyFirstBuild = true;

        if (error) {
            return;
        }
    }

    let webpackOptions = {
        output: {
            publicPath: '/js/',
            filename: isDevelopment ? '[name].js' : '[name]-[chunkhash:10].js'
        },
        watch: isDevelopment,
        devtool: isDevelopment ? 'cheap-module-inline-source-map' : null,
        module: {
            loaders: [{
                test: /\.js$/,
                include: path.join(__dirname, 'app'),
                loaders: ['ng-annotate', 'babel?presets[]=es2015']
            }, {
                test: /\.html$/,
                loader: 'raw'
            }]
        },
        plugins: [
            new webpack.NoErrorsPlugin()
        ]
    };

    if (!isDevelopment) {
        webpackOptions.plugins.push(new AssetsPlugin({
                filename: 'webpack.json',
                path: __dirname + '/manifest',
                processOutput(assets) {
                    for (let key in assets) {
                        assets[key + '.js'] = assets[key].js.slice(webpackOptions.output.publicPath.length);

                        delete assets[key];
                    }

                    return JSON.stringify(assets);
                }
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: true,
                    drop_console: true, //delete console.log etc.
                    unsafe: true //use unsafe features
                }
            })
        );
    }

    return gulp.src('app/js/index.js')
        .on('error', function (error) {
            console.error(error);

            this.end();
        })
        .pipe(webpackStream(webpackOptions, null, done))
        .pipe(gulp.dest('build/js'))
        .on('data', () => {
            if (isReadyFirstBuild && !callback.called) {
                callback.called = true;
                callback();
            }
        });
});

gulp.task('clean', () => {
    return del(['build', 'manifest']);
});

gulp.task('build', gulpSequence('clean', 'assets', 'webpack'));

gulp.task('watch', () => {
    gulp.watch('app/templates/**/*.*', ['assets']);
});

gulp.task('serve', () => {
    browserSync.init({
        server: 'build'
    });

    browserSync.watch('build/**/*.*').on('change', browserSync.reload);
});

gulp.task('dev', gulpSequence('build', ['watch', 'serve']));