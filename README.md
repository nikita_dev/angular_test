# README #

Before running the project, please make sure that NodeJS is installed on your computer.

In order to build and run the project please run the following command: npm install -g gulp.

To run unit-tests please install karma by following command: npm install -g karma-cli, to run tests please use the following command: karma start.

To run the project please run the following commands from project root: npm install; gulp dev.
 
# Interface #

To move block click on it.