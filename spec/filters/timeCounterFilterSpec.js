describe('filter: timeCounter', function() {
    beforeEach(module('angularApp'));

    it('should convert milliseconds to seconds with decimals',
        inject(function (timeCounterFilter) {
            expect(timeCounterFilter(1234)).toEqual('123.4');
        })
    );
});