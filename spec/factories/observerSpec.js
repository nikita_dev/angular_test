describe('factory: observer', function() {
    let observer;

    beforeEach(function () {
        module('angularApp');

        inject(function ($injector) {
            observer = $injector.get('observerFactory');
        });
    });

    it('should register callback', function () {
        let callback = function () {
            return 2 + 2;
        };

        observer.registerCallback('test', callback);

        expect(Object.keys(observer.getCallbacks()).length).toEqual(1);
        expect(observer.getCallbacks()).toEqual({
            'test': callback
        });
    });

    it('should call callback', function () {
        let callback = jasmine.createSpy();

        observer.registerCallback('test', callback);

        observer.notify('test');

        expect(callback).toHaveBeenCalledTimes(1);
    });
});