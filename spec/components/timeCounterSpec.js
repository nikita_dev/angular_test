describe('component: stepsCounter', function() {
    let $componentController;
    let observerFactory;
    let $interval;

    beforeEach(function() {
        module('angularApp');

        inject(function ($injector) {
            observerFactory = $injector.get('observerFactory');
            $interval = $injector.get('$interval');
        });
    });
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should increment time counter every 100 milliseconds', function() {
        let ctrl = $componentController('timeCounter');

        expect(ctrl.value).toEqual(0);

        $interval.flush(50);

        expect(ctrl.value).toEqual(0);

        $interval.flush(50);

        expect(ctrl.value).toEqual(1);
    });

    it('should reset time counter on restartTimeCounter callback', function() {
        let ctrl = $componentController('timeCounter');

        expect(ctrl.value).toEqual(0);

        $interval.flush(100);

        expect(ctrl.value).toEqual(1);

        observerFactory.notify('restartTimeCounter');

        expect(ctrl.value).toEqual(0);
    });

    it('should stop time counter on stopTimeCounter callback', function() {
        let intervalSpy = jasmine.createSpy('$interval', $interval).and.callThrough();
        let ctrl = $componentController('timeCounter', {$interval: intervalSpy});

        spyOn(intervalSpy, 'cancel');

        expect(ctrl.value).toEqual(0);

        observerFactory.notify('stopTimeCounter');

        expect(intervalSpy.cancel.calls.count()).toEqual(1);
    });
});