describe('component: block', function() {
    let $componentController;

    beforeEach(module('angularApp'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should have a number', function() {
        let bindings = {number: 2};
        let ctrl = $componentController('block', null, bindings);

        expect(ctrl.number).toEqual(2);
    });

    it('should call the v binding, when moving a block', function() {
        let onMoveSpy = jasmine.createSpy('onMove');
        let bindings = {number: 2, onMove: onMoveSpy};
        let ctrl = $componentController('block', null, bindings);

        ctrl.move();
        expect(onMoveSpy).toHaveBeenCalled();
    });
});