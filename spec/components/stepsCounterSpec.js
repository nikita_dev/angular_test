describe('component: stepsCounter', function() {
    let $componentController;
    let observerFactory;

    beforeEach(function() {
        module('angularApp');

        inject(function ($injector) {
            observerFactory = $injector.get('observerFactory');
        });
    });
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should register 2 callbacks', function() {
        $componentController('stepsCounter');

        expect(Object.keys(observerFactory.getCallbacks()).length).toEqual(2);
    });

    it('should increment value on step callback call, set value to 0 on resetStepCounter callback call', function () {
        let ctrl = $componentController('stepsCounter');

        expect(ctrl.value).toEqual(0);

        observerFactory.notify('step');

        expect(ctrl.value).toEqual(1);

        observerFactory.notify('resetStepCounter');

        expect(ctrl.value).toEqual(0);
    });
});