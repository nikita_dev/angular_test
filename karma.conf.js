// Karma configuration
// Generated on Tue Apr 25 2017 20:39:39 GMT+0300 (Финляндия (лето))

const path = require('path');

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        './node_modules/angular/angular.js',
        './node_modules/angular-mocks/angular-mocks.js',
        './app/js/main.js',
        './app/js/filters/timeCounterFilter.js',
        './app/js/factories/observer.js',
        './app/js/components/block.js',
        './app/js/components/stepsCounter.js',
        './app/js/components/timeCounter.js',
        './spec/filters/timeCounterFilterSpec.js',
        './spec/factories/observerSpec.js',
        './spec/components/blockSpec.js',
        './spec/components/stepsCounterSpec.js',
        './spec/components/timeCounterSpec.js',
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'app/**/*.js': ['webpack'],
    },

    webpack: {
        module: {
            loaders: [{
                test: /\.html$/,
                loader: 'raw'
            }]
        },
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
