export default angular.module('angularApp').factory('observerFactory', function () {
    let observerCallbacks = {};

    this.registerCallback = function (name, callback) {
        observerCallbacks[name] = callback;
    };

    this.notify = function (name, args) {
        angular.isFunction(observerCallbacks[name]) && observerCallbacks[name](args);
    };

    this.getCallbacks = function () {
        return observerCallbacks;
    };

    return this;
});