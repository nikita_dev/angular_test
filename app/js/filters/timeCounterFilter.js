export default angular.module('angularApp').filter('timeCounter', function ($filter) {
    return function (input) {
        if (!angular.isNumber(input)) {
            return input;
        }

        return $filter('number')(input / 10, 1);
    }
});