export default angular.module('angularApp').component('block', {
    templateUrl: '../block.html',
    bindings: {
        number: '<',
        onMove: '&'
    },
    controller: function () {
        let vm = this;

        vm.move = function () {
            vm.onMove();
        };
    }
});