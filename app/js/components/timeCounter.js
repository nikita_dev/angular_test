export default angular.module('angularApp').component('timeCounter', {
    templateUrl: '../timeCounter.html',
    controller: function ($interval, observerFactory) {
        let vm = this;

        vm.value = 0;

        let timeCounter = $interval(function () {
            vm.value++;
        }, 100);

        observerFactory.registerCallback('restartTimeCounter', function () {
            vm.value = 0;
        });

        observerFactory.registerCallback('stopTimeCounter', function () {
            $interval.cancel(timeCounter);
        });
    }
});