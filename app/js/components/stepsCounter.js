export default angular.module('angularApp').component('stepsCounter', {
    templateUrl: '../stepsCounter.html',
    controller: function (observerFactory) {
        let vm = this;

        vm.value = 0;

        observerFactory.registerCallback('step', function () {
            vm.value++;
        });

        observerFactory.registerCallback('resetStepCounter', function () {
            vm.value = 0;
        });
    }
});