import angular from 'angular';

export default angular.module('angularApp').component('board', {
    templateUrl: '../board.html',
    controller: function ($scope, observerFactory) {
        let vm = this;

        vm.level = '15';

        let getSize = function () {
            return Math.sqrt(parseInt(vm.level) + 1);
        };

        let shuffleBlocks = function (board) {
            let i = board.length, j, tempi, tempj;

            while (--i) {
                j = Math.floor(Math.random() * (i + 1));
                tempi = board[i];
                tempj = board[j];
                board[i] = tempj;
                board[j] = tempi;
            }
        };

        let change = function (oldRow, oldCol, newRow, newCol) {
            let temp = vm.board[oldRow][oldCol];

            vm.board[oldRow][oldCol] = vm.board[newRow][newCol];
            vm.board[newRow][newCol] = temp;

            observerFactory.notify('step');
        };

        let tryToChange = function (row, col) {
            switch (0) {
                case vm.board[row][col - 1]: {
                    change(row, col, row, col - 1);

                    break;
                }
                case vm.board[row][col + 1]: {
                    change(row, col, row, col + 1);

                    break;
                }
                case angular.isArray(vm.board[row - 1]) && vm.board[row - 1][col]: {
                    change(row, col, row - 1, col);

                    break;
                }
                case angular.isArray(vm.board[row + 1]) && vm.board[row + 1][col]: {
                    change(row, col, row + 1, col);

                    break;
                }
            }
        };

        let isSolved = function () {
            let buffer = [];

            for (let i = 0; i < vm.board.length; i++) {
                buffer = buffer.concat(vm.board[i]);
            }

            for (let j = 0; j < buffer.length; j++) {
                if (buffer[j] !== j + 1) {
                    return false;
                }
            }

            return true;
        };

        vm.restart = function () {
            observerFactory.notify('resetStepCounter');

            observerFactory.notify('restartTimeCounter');

            vm.board = vm.getBoard();
        };

        vm.onMove = function (number) {
            let found = false;

            for (let i = 0; i < vm.board.length; i++) {
                for (let j = 0; j < vm.board[i].length; j++) {
                    if (vm.board[i][j] === number) {
                        tryToChange(i, j);

                        found = true;

                        break;
                    }
                }

                if (found) {
                    break;
                }
            }

            if (isSolved()) {
                observerFactory.notify('stopTimeCounter');

                alert('Congratulations!');

                observerFactory.notify('restartTimeCounter');
            }
        };

        vm.getBoard = function () {
            let buffer = Array.from(Array(parseInt(vm.level) + 1).keys()),
                result = [];

            shuffleBlocks(buffer);

            for (let i = 0; i < getSize(); i++) {
                result.push(
                    buffer.slice(getSize() * i, getSize() * (i + 1))
                );
            }

            return result;
        };

        $scope.$watch(function () {
            return vm.level;
        }, function () {
            observerFactory.notify('restartTimeCounter');

            observerFactory.notify('resetStepCounter');

            vm.board = vm.getBoard();
        });
    }
});