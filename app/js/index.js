import angular from 'angular';

import './main';
import './factories/observer';
import './filters/timeCounterFilter';
import './components/board';
import './components/stepsCounter';
import './components/block';
import './components/levelSelector';
import './components/timeCounter';

angular.element(function() {
    angular.bootstrap(document, ['angularApp']);
});